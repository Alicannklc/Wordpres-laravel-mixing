
// Import IE polyfill for Vue
import "babel-polyfill";

// Load JavaScript dependencies
require('./bootstrap');

// Vue
window.Vue = require('vue');

Vue.component('example', require('./components/Example.vue'));

var app = new Vue({
  el: '#app'
});
